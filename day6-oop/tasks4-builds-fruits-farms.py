# -*- coding: utf-8 -*-
import re


class Fruit(object):
    def __init__(self, s):
        self.s = s

    def get_fruits_count(self, fruit):
        n = 0
        sclone = self.s[:]
        while True:
            f_i = sclone.find(fruit)
            if f_i == -1:
                return n
            n_i1 = sclone.find(' ', f_i)
            n_i2 = sclone.find('.', f_i)
            n += int(sclone[n_i1:n_i2])
            sclone = sclone[n_i2:]

    def get_sum_count(self):
        return sum(map(int, re.findall(': (\d+)\.', self.s)))

    def get_all_fruits(self):
        r = re.compile(u' (\w+?): ', re.UNICODE)
        fruits = r.findall(self.s.decode('utf-8'))
        fruits = list(set(fruits))
        result = {}
        for fruit in fruits:
            result[fruit] = self.get_fruits_count(fruit.encode('utf-8'))
        return result

print '### Фрукты'
fs = 'Яна съела яблок: 3. Анюта съела яблок: 4. '
fs += 'Андрей съел дынь: 5. Максим съел яблок: 2.'
f = Fruit(fs)
print 'Количество яблок: %s' % f.get_fruits_count('яблок')
print 'Общее количество фруктов: %s' % f.get_sum_count()
fobj = f.get_all_fruits()

print 'Возвращаем словарь с каждым видом фруктов:'
for k, v in fobj.iteritems():
    print '\t', k, v
