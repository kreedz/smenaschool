import math
import hashlib


class Point(object):
    def __init__(self, x, y, str_=0):
        self.x = x
        self.y = y
        self.str_ = str_

    def __str__(self):
        if self.str_ == 0:
            s = 'Point at %s, %s' % (self.x, self.y)
        elif self.str_ == 1:
            s = '%s, %s' % (self.x, self.y)
        return s


class Rectangle(object):
    def __init__(self, xy1, xy2):
        self.xy1 = xy1
        self.xy2 = xy2

    def set_width_height(self):
        p = self.get_width_height()
        self.w = p[0]
        self.h = p[1]

    def get_rect_coord_str(self):
        return 'Rectangle at (%s, %s)' % self.xy1 + ' (%s, %s)' % self.xy2

    def __str__(self):
        self.set_width_height()
        r = self.get_rect_coord_str()
        r += '\nwidth: %s, height: %s' % (self.w, self.h)
        r += '\nperimeter: %s' % self.get_perimeter()
        r += '\nsquare: %s' % self.get_area()
        r += '\nIs it square? %s\n' % self.is_square()
        return r

    def get_width_height(self):
        return self.xy1[1] - self.xy2[1], self.xy2[0] - self.xy1[0]

    def get_perimeter(self):
        return self.w * 2 + self.h * 2

    def get_area(self):
        return self.w * self.h

    def is_square(self):
        return self.w == self.h


class RectangleInherite(Rectangle):
    def __init__(self, xy1, w, h):
        super(RectangleInherite, self).__init__(xy1, (xy1[0] + w, xy1[1] - h))
        self.xy1 = xy1
        self.w = w
        self.h = h
        if w < 0 or h < 0:
            raise Exception('Width and height must be more than 0!')
        self.xy2 = (xy1[0] + w, xy1[1] - h)


class RectanglePoint(Rectangle):
    def __init__(self, xy1, xy2):
        super(RectanglePoint, self).__init__(xy1, xy2)

    def get_width_height(self):
        return self.xy1.y - self.xy2.y, self.xy2.x - self.xy1.x

    def get_rect_coord_str(self):
        arg1 = (self.xy1.x, self.xy1.y)
        arg2 = (self.xy2.x, self.xy2.y)
        return 'Rectangle at (%s, %s)' % arg1 + ' (%s, %s)' % arg2


class Circle(object):
    def __init__(self, c, r):
        self.c = c
        self.r = r

    def get_diameter(self):
        return self.r + self.r

    def get_length(self):
        return 2 * math.pi * self.r

    def get_square(self):
        return math.pi * self.r * self.r

    def __str__(self):
        arg = self.get_diameter(), self.get_length(), self.get_square()
        return '\nDiameter: %s\nLength: %s\nSquare: %s\n' % arg


class Student(object):
    def __init__(self, fio, date, number, courses):
        self.fio = fio
        self.date = date
        self.number = number
        self.courses = courses
        self.id_ = hashlib.md5(self.fio + self.date + self.number)

    def get_courses_count(self):
        count = 0
        for course in self.courses:
            if self.id_ in course.get_id_students():
                count += 1
        return count

    def __str__(self):
        courses_count = self.get_courses_count()
        arg = self.fio, self.date, self.number, courses_count
        s = 'Student\'s name: %s, birthday: %s, student\'s number: %s, '
        s += 'courses\' count: %s'
        return s % arg


class Course(object):
    def __init__(self, name, number):
        self.name = name
        self.number = number
        self.students = []

    def get_students_count(self):
        return len(self.students)

    def get_id_students(self):
        ids = []
        for student in self.students:
            ids.append(student.id_)
        return ids

    def add_student(self, student):
        self.students.append(student)

    def __str__(self):
        arg = self.name, self.number, self.get_students_count()
        return 'Course name: %s, cabinet: %s, students\' count: %s' % arg


class Teacher(object):
    def __init__(self, name, course):
        self.name = name
        self.course = course

    def __str__(self):
        students = ', '.join([student.fio for student in self.course.students])
        arg = self.name, self.course.name, students
        return 'Teacher: %s, course: %s\n\tstudents: %s' % arg

print '### task 1'
p = Point(3, 4)
print p

print '### task 2'
r = Rectangle((3, 4), (6, 0))
print r

print '### task 3'
ri = RectangleInherite((3, 4), 3, 4)
print ri

print '### task 4'
rp = RectanglePoint(Point(3, 4, str_=1), Point(6, 0, str_=1))
print rp

print '### task 5'
c = Circle(Point(3, 3), 3)
print c

print '### task 6'
courses = [
    Course('Course1', '1'),
    Course('Course2', '2'),
    Course('Course3', '3'),
]
students = [
    Student('fio1', '11.11.11', '111', courses),
    Student('fio2', '22.01.10', '222', courses),
    Student('fio3', '12.02.09', '333', courses),
    Student('fio4', '13.03.08', '444', courses),
    Student('fio5', '14.04.07', '555', courses),
]

courses[0].add_student(students[0])
courses[1].add_student(students[0])
courses[2].add_student(students[0])
courses[0].add_student(students[1])
courses[1].add_student(students[2])

for course in courses:
    print course

for student in students:
    print student

print '\n### task 7'
teachers = [
    Teacher('Teacher1', courses[0]),
    Teacher('Teacher2', courses[1]),
    Teacher('Teacher3', courses[2]),
]

for teacher in teachers:
    print teacher
