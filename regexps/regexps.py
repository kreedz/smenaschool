import re


class ValidatePassword(object):
    def __init__(self, password):
        self.password = password

    def check(self, regexp):
        return re.search(regexp, self.password) is not None

    def is_digit_min_4(self):
        return self.check('^\d{4,}$')

    def is_latin_low_len_6_10(self):
        return self.check('^[a-z]{6,10}$')

    def is_latin_low_up_digit_special_chars_len_6_10(self):
        return self.check('^[\w@.-]{6,10}$')

    def is_contains_all_symbols(self):
        return self.check('(?=.*\d+)(?=.*[a-z]+)(?=.*[A-Z])(?=.*[@._-]+)')
        # digit = self.check('\d+')
        # low = self.check('[a-z]+')
        # up = self.check('[A-Z]+')
        # special = self.check('[@._-]+')
        # return bool(digit * low * up * special)

    def run_all(self):
        print '\tPassword: %s' % self.password
        print 'Only digit, min len 4: %s' % self.is_digit_min_4()
        print 'Only latin, range len 6-10: %s' % self.is_latin_low_len_6_10()
        print 'Allow latin, lower upper chars, digit, special chars, len 6-10: %s' %\
            self.is_latin_low_up_digit_special_chars_len_6_10()
        print 'All symbols'' type is required. ' + \
            'Only latin, lower upper chars, digit, special chars, len 6-10: %s' %\
            self.is_contains_all_symbols()


def clear_phone(phone):
    return ''.join(re.findall('\d+', phone))[1:]


def is_validate_email(email):
    return re.match('^[\w-]+@[\w-]+\.[\w-]+$', email) is not None


def run_tasks():
    print '### Task 1. Validate password'
    passwords = ['qwe123A-._@', '1234', 'abcdefgh', 'abAB12@-']
    for password in passwords:
        ValidatePassword(password).run_all()
    phones = ['+7 961 (123) 123-54', '89610456317', '+7  (12) (32) (4)-(56)91']

    print '\n### Task 2. Clear phone number.'
    for phone in phones:
        args = (phone, clear_phone(phone))
        print 'Phone number: %s\nCleared number: %s' % args

    print '\n### Task 3.'
    emails = ['test@test.com', 'qw@asd@.com.com', 'test@', 'a1@a.a']
    for email in emails:
        args = (email, is_validate_email(email))
        print 'Is email (%s) validated?: %s' % args


run_tasks()
