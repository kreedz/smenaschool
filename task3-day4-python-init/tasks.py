import math


def get_2():
    return 2


def get_cube(a):
    return a * a * a


def get_fact(a):
    return 1 if a == 0 else get_fact(a - 1) * a


def get_less_numb(a, b):
    return a if a < b else b


def get_less_count_numb(a, b):
    if abs(a) < abs(b):
        return a
    elif abs(a) > abs(b):
        return b
    return -1


def get_hipoten(a, b):
    return math.sqrt(a * a + b * b)


def get_last_numb(a):
    return a % 10


def get_sum_numb3(a):
    sum_ = 0
    for i in range(3):
        sum_ += a % 10
        a /= 10
    return sum_


def get_sum_numb(a):
    sum_ = 0
    while a:
        sum_ += a % 10
        a /= 10
    return sum_


def get_count_numb_eq_2(a):
    c = 0
    for i in a:
        if i == 2:
            c += 1
    return c


def get_count_numb_eq(l, a):
    c = 0
    for i in l:
        if i == a:
            c += 1
    return c


def get_count_numb_eq_default(l, a=2):
    return get_count_numb_eq(l, a)


def get_count_numb_eq_diff_type(l, a=1.):
    c = 0
    for i in l:
        if type(i) == type(a) and i == a:
            c += 1
    return c


def get_longest_str(*ss):
    return get_longest_str_tpl(*ss)[-1]


def get_longest_str_tpl(*ss):
    return max(map(lambda s: (len(s), s), ss))


def is_4_year(year):
    if year % 400 == 0:
        return True
    if year % 100 == 0:
        return False
    if year % 4 == 0:
        return True
    else:
        return False


def get_cycle_list(l):
    j = 0
    b = l[:]
    for i in l:
        j += 1
        if j == len(l):
            break
        b[j] = i
    b[0] = l[-1]
    return b

s = '\n%s:\n'
print s % 1, get_2()
print s % 2, get_cube(3)
print s % 3, get_fact(4)
print s % 4, get_less_numb(3, 5)
print s % 5, get_less_count_numb(23123, 123)
print s % 6, get_hipoten(8, 9)
print s % 7, get_last_numb(123)
print s % 8, get_sum_numb3(123)
print s % 9, get_sum_numb(12345)
print s % 10, get_count_numb_eq_2([1, 2, 3, 4, 3, 2])
print s % 11, get_count_numb_eq([1, 2, 3, 2], 2)
print s % 12, get_count_numb_eq_default([1, 2, 3, 2])
print s % 13, get_count_numb_eq_diff_type([1., None, 1, 'a', 'bb', 'a'])
print s % 14, get_longest_str('asd', 'q', 'zxc', 'zxcasd')
print s % 15, get_longest_str_tpl('asd', 'q', 'zxc', 'zxcasd')
print s % 16, is_4_year(2016)
arg17 = [1, 2, 3, 4]
print s % 17, 'Original list: ' + str(arg17)
print 'Cycled list: ' + str(get_cycle_list(arg17))
