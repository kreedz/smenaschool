# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render_to_response

from datetime import date, datetime, timedelta

from .news import News


def today(request):
    return HttpResponse(datetime.strftime(date.today(), '%d %B %Y'))


def date_by_weekday(request):
    today = request.GET.get('weekday').lower()
    dt = date.today()
    start = dt - timedelta(days=dt.weekday())
    result = None
    for i in range(7):
        check_date = start + timedelta(days=i)
        if check_date.strftime('%A').lower() == today:
            result = check_date
            break

    return HttpResponse(result.strftime('%d.%m.%Y'))


def weekday_by_date(request):
    today = request.GET.get('date')
    return HttpResponse(datetime.strptime(today, '%d.%m.%Y').strftime('%A'))


def date_range(requests):
    date_begin = datetime.strptime(requests.GET.get('date-begin'), '%d.%m.%Y')
    date_end = datetime.strptime(requests.GET.get('date-end'), '%d.%m.%Y')
    return HttpResponse(abs((date_begin - date_end).days))


def payment_ndfl(requests):
    salary = int(requests.GET.get('salary')) * 0.13
    return HttpResponse(salary)


def payment_pension(requests):
    salary = int(requests.GET.get('salary')) * 0.22
    return HttpResponse(salary)


def payment_other_pay(requests):
    salary = requests.GET.get('salary')
    social = int(salary) * 0.029
    medical = int(salary) * 0.051
    tax = int(salary) * 0.002
    socialt = 'Фонд социального страхования: ' + str(social)
    medicalt = '<br>Федеральный фонд обязательного медицинского страхования: '
    medicalt += str(medical)
    taxt = '<br>Страховой налог от несчастного случая на производстве для'
    taxt += ' служащих: '
    taxt += str(tax)
    pension = int(salary) * 0.22
    sumt = '<br>Итого: ' + str(social + medical + tax + pension)
    return HttpResponse(socialt + medicalt + taxt + sumt)


def index(requests):
    context = {'a': 1}
    return render_to_response(u'index.html', context=context)


def news(requests):
    context = {'news_items': sorted(News().get_all_news().iteritems())}
    return render_to_response(u'news.html', context=context)


def news_item(requests, news_header):
    context = {
        'news_content': News().get_news_item(news_header),
        'news_header': news_header,
    }
    return render_to_response(u'news_item.html', context=context)
