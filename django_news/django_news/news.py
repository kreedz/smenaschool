import re


def get_first_nth_words(text, count=30):
    return ' '.join(re.split('\s', text.strip())[:count+1]) + '...'


class News(object):
    def __init__(self):
        self.news = {
            'News 1': '''
            Great Britain won two swimming silver medals on day four of the Olympics thanks to Siobhan-Marie O'Connor and the men's 4x200m freestyle relay team.

    O'Connor broke the British record as she finished behind Hungary's Katinka Hosszu in the 200m individual medley.

    The relay team were second behind the USA for GB's sixth medal in Rio.

    Michael Phelps swam the final leg for the Americans as he claimed his 21st Olympic title, having earlier won the 200m butterfly.

    Away from the competition, a bus carrying journalists between venues had two windows smashed after stones were thrown at it, while it was confirmed Irish boxer Michael O'Reilly had failed a drugs test and will not fight at the Games.
            ''',
            'News 2': '''
    The Brazilian Senate has voted to hold an impeachment trial of suspended President Dilma Rousseff, who is accused of breaking the budget law.

    The senate voted 59 to 21 in favour of going ahead with the trial against Ms Rousseff following a marathon debate that ended early on Wednesday.

    The Senate suspended Ms Rousseff in May over alleged illegal accounting practices.

    She says they were common practice under previous administrations.

    The senate required a simple majority to decide on whether to try Ms Rousseff. A two-thirds majority is needed in the final vote following the trial, which is due in the week after the Olympics closing ceremony.

    As the debate got under way on Tuesday, Supreme Court President Ricardo Lewandowski told senators that they were about to "exercise one of the most serious tasks under the constitution".

    Ms Rousseff has been accused of spending money without congressional approval and taking out unauthorised loans from state banks to boost the national budget ahead of the 2014 election, when she was re-elected.
            ''',
            'News 3': '''
            Brazil real life house of cards

    Ms Rousseff is not facing corruption charges in Brazil's wide-ranging scandal around the state oil company, Petrobras.

    But she has been tainted by the scandal, in which her Workers' Party is accused of lining its campaign war chests with some of the missing money.

    If she is removed from office, the interim president, her former running mate Michel Temer, will remain in the presidential chair until the next elections in 2018.

    Ms Rousseff has accused him of orchestrating a political coup against her.

    At the Olympics opening ceremony on Friday, Mr Temer drew boos from the crowds as he declared the games open.

    There have been various protests against him before the games as well as peaceful protests at a number of Olympic venues.
            ''',
        }

    def get_all_news(self):
        news = self.news.copy()
        for header, content in news.iteritems():
            news[header] = get_first_nth_words(content)
        return news

    def get_news_item(self, news_header):
        return self.news[news_header]
