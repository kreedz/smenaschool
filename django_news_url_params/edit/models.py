from __future__ import unicode_literals

from django.db import models


class Editor(models.Model):
    user = models.OneToOneField('auth.User')

    def __str__(self):
        return 'Editor: %s' % self.user.username
