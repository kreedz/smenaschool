$(document).ready(function(){
    $('.buttons-color').on('click', function(e){
        $('#block-color-change').css('background-color', $(this).css('background-color'));
    });
    $('#block-color-change').on('click', function(e){
        $(this).css('background-color', "#"+((1<<24)*Math.random()|0).toString(16));
    });

    function set_result(result) {
        $('input[name="result"]').val(result);
    }
    $('.operation').on('click', function(e){
        var value1 = parseInt($('input[name="field1"]').val());
        var value2 = parseInt($('input[name="field2"]').val());
        switch($(this).text()){
            case '+':
                set_result(value1 + value2);
                break;
            case '-':
                set_result(value1 - value2);
                break;
            case '/':
                set_result(value1 / value2);
                break;
            case '*':
                set_result(value1 * value2);
                break;
        }
    });
});
