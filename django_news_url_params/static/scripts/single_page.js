$(document).ready(function(){
    var csrftoken = Cookies.get('csrftoken');
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    function unset_news_background(news_body){
        news_body.find('td').each(function(){
            $(this).css('background-color', 'white');
        });
    }

    function check_bookmark_exist(news){
        return $.ajax({
            url: 'is-bookmark-exists/',
            type: 'GET',
            data: {'news': news},
        });
    }

    function show_add_rm_btns(btn) {
        var bookmark_add_btn = $('#clickable-add-bookmark');
        var bookmark_rm_btn = $('#clickable-rm-bookmark');
        if (btn == 'add') {
            bookmark_rm_btn.hide();
            bookmark_add_btn.show();
        } else if (btn == 'rm') {
            bookmark_add_btn.hide();
            bookmark_rm_btn.show();
        }
    }

    function bookmark_process(news, user){
        check_bookmark_exist(news).done(function(data){
            if (data['is_exist']) {
                show_add_rm_btns('rm');
                $('#clickable-rm-bookmark').off('click').on('click', function(e){
                    $.ajax({
                        url:'rm-bookmark/by-news/' + news,
                        type: 'DELETE',
                    }).done(function(data){
                        show_add_rm_btns('add');
                        alert('Закладка удалена');
                    }).fail(function(xhr, status, error){
                        if (typeof xhr.responseJSON != 'undefined'){
                            alert(xhr.responseJSON.errors.__all__);
                        }
                    });
                });
            } else {
                show_add_rm_btns('add');
            }
        });
    }

    function set_entities(entities_name, news_body){
        if (entities_name == 'bookmarks' || entities_name == 'news') {
            var get_url = entities_name;
        }
        var fields_names = get_fields_names(entities_name);

        $.get('get-' + get_url, function(data){
            news_body.empty();
            data['news'].forEach(function(item){
                var td = $('<td>').text(item[fields_names['name']]);
                td.data('id', item[fields_names['id']]);
                td.on('click', function(e){
                    $.get('get-news/' + td.data('id') + '/text', function(data){
                        var user_id = $('#single-page').data('user-id');
                        var news_id = data['id'];
                        bookmark_process(news_id, user_id);

                        $('#single-page-news-text').text(data['text']);
                        $('#single-page-news-text').data('id', news_id);
                        $('#single-page-news-header').text(item[fields_names['name']]);
                        unset_news_background(news_body);
                        $(e.currentTarget).css('background-color', 'grey');
                    });
                });
                var tr = $('<tr>').append(td);
                news_body.append(tr);
            });
            news_body.find('tr:first td').trigger('click');
        });
    }

    function add_bookmark(){
        var news_id = $('#single-page-news-text').data('id');
        var user_id = $('#single-page').data('user-id');
        $.post('add-bookmark/', {'news': news_id, 'user': user_id}, function(data){
            alert('Закладка добавлена');
            show_add_rm_btns('rm');
        }).fail(function(xhr, status, error){
            alert('Закладка не добавлена. Либо вы не являетесь пользователем, либо это внутренняя ошибка приложения!')
            // alert(xhr.responseJSON.errors.__all__);
        });
    }

    function get_fields_names(entities_name) {
        if (entities_name == 'news') {
            var id = 'id';
            var name = 'name';
        } else if (entities_name == 'bookmarks') {
            var id = 'news__id';
            var name = 'news__name';
        }
        return {'id': id, 'name': name};
    }

    function toggle_underlines() {
        var cls = 'text-underline';
        var news_header = $('td#clickable-news');
        var bookmark_header = $('td#clickable-bookmark');
        if (news_header.hasClass(cls)) {
            $(news_header).toggleClass(cls);
        }
        if (bookmark_header.hasClass(cls)) {
            $(bookmark_header).toggleClass(cls);
        }
    }

    var news_body = $('#single-page-news tbody');
    set_entities('news', news_body);

    $('td#clickable-news').on('click', function(e){
        set_entities('news', news_body);
        toggle_underlines();
        $(this).toggleClass('text-underline');
    });
    $('td#clickable-add-bookmark').on('click', function(e){
        add_bookmark();
    });
    $('td#clickable-bookmark').on('click', function(e){
        set_entities('bookmarks', news_body);
        set_possibility_to_remove_bookmark();
        toggle_underlines();
        $(this).toggleClass('text-underline');
    });
});
