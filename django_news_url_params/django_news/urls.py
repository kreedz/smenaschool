"""django_news URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.views.generic import TemplateView

from . import views


urlpatterns = [
    url(r'^edit/', include('edit.urls', namespace='edit')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^date/today', views.today),
    url(r'^date-by-weekday/(?P<weekday>[a-zA-Z]+)', views.date_by_weekday),
    url(r'^weekday-by-date/'
        '(?P<date>date\(\d{2}-\d{2}-\d{4}\))', views.weekday_by_date),
    url(r'^date-range/(?P<date_begin>date-begin\(\d{2}-\d{2}-\d{4}\))'
        '/(?P<date_end>date-end\(\d{2}-\d{2}-\d{4}\))', views.date_range),
    url(r'^payment/ndfl/(?P<salary>\d+)', views.payment_ndfl),
    url(r'^payment/pension/(?P<salary>\d+)', views.payment_pension),
    url(r'^payment/other-pay/(?P<salary>\d+)', views.payment_other_pay),
    url(r'^$', views.IndexTemplateView.as_view(), name='index'),
    url(r'^news$', views.NewsTemplateView.as_view(), name='news'),
    url(ur'^news/(?P<slug>.+)$',
        views.NewsDetailView.as_view(), name='news_item'),
    url(r'^news/category/by-viewed$',
        views.NewsByViewedListView.as_view(), name='news_by_viewed'),
    url(r'^news/category/by-latest$',
        views.NewsByLatestListView.as_view(), name='news_by_latest'),
    url(r'^news/category/by-comments$',
        views.NewsByCommentsListView.as_view(), name='news_by_comments'),
    url(r'^registration',
        views.RegistrationFormView.as_view(), name='registration'),
    url(r'^login', views.LoginFormView.as_view(), name='login'),
    url(r'^logout', views.LogoutView.as_view(), name='logout'),
    url(r'^personal$', views.PersonalListView.as_view(), name='personal'),
    url(r'^about$',
        TemplateView.as_view(template_name='about.html'), name='about'),
    url(r'^jquery_tasks$',
        TemplateView.as_view(template_name='jquery_tasks.html'),
        name='jquery_tasks'),
    url(r'^single-page',
        views.SinglePageListView.as_view(), name='single_page'),
    url(r'^get-news/$',
        views.SinglePageJSONNewsListView.as_view(), name='get_news'),
    url(r'^get-news/(?P<pk>\d+)/text',
        views.SinglePageJSONNewsDetailView.as_view(), name='get_news_text'),
    url(r'^add-bookmark',
        views.SinglePageJSONBookmarkCreateView.as_view(), name='add_bookmark'),
    url(r'^get-bookmarks/$',
        views.SinglePageJSONBookmarksListView.as_view(), name='get_bookmarks'),
    url(r'^is-bookmark-exists/$',
        views.SinglePageJSONBookmarkExistsDetailView.as_view(),
        name='is_bookmark_exists'),
    url(r'^rm-bookmark/by-news/(?P<news>\d+)$',
        views.SinglePageJSONBookmarkDeleteView.as_view(), name='rm_bookmarks'),
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
