# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render

from django.db.models import F
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from django.db.models.aggregates import Count

from django.views import View
from django.views.generic import TemplateView, ListView, DetailView, FormView
from django.views.generic.edit import BaseCreateView, BaseDeleteView
from django.views.generic.list import BaseListView
from django.views.generic.detail import BaseDetailView
from django.contrib.auth.mixins import LoginRequiredMixin

from datetime import date, datetime, timedelta
import re

from models import News, Comment, Bookmark
from .forms import RegistrationForm, CommentModelForm, LoginForm
from .forms import BookmarkModelForm


def today(request):
    return HttpResponse(datetime.strftime(date.today(), '%d %B %Y'))


def date_by_weekday(request, weekday):
    dt = date.today()
    start = dt - timedelta(days=dt.weekday())
    result = None
    for i in range(7):
        check_date = start + timedelta(days=i)
        if check_date.strftime('%A').lower() == weekday:
            result = check_date
            break

    return HttpResponse(result.strftime('%d.%m.%Y'))


def weekday_by_date(request, date):
    date = re.search('\((.*)\)', date).group(1)
    return HttpResponse(datetime.strptime(date, '%d-%m-%Y').strftime('%A'))


def date_range(requests, date_begin, date_end):
    date_begin = re.search('\((.*)\)', date_begin).group(1)
    date_begin = datetime.strptime(date_begin, '%d-%m-%Y')
    date_end = re.search('\((.*)\)', date_end).group(1)
    date_end = datetime.strptime(date_end, '%d-%m-%Y')
    return HttpResponse(abs((date_begin - date_end).days))


def payment_ndfl(requests, salary):
    return HttpResponse(int(salary) * 0.13)


def payment_pension(requests, salary):
    return HttpResponse(int(salary) * 0.22)


def payment_other_pay(requests, salary):
    social = int(salary) * 0.029
    medical = int(salary) * 0.051
    tax = int(salary) * 0.002
    socialt = 'Фонд социального страхования: ' + str(social)
    medicalt = '<br>Федеральный фонд обязательного медицинского страхования: '
    medicalt += str(medical)
    taxt = '<br>Страховой налог от несчастного случая на производстве для'
    taxt += ' служащих: '
    taxt += str(tax)
    pension = int(salary) * 0.22
    sumt = '<br>Итого: ' + str(social + medical + tax + pension)
    return HttpResponse(socialt + medicalt + taxt + sumt)


class IndexTemplateView(TemplateView):
    template_name = u'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexTemplateView, self).get_context_data(**kwargs)
        context['user'] = self.request.user
        return context


class NewsTemplateView(TemplateView):
    template_name = u'news.html'

    def get_context_data(self, **kwargs):
        context = super(NewsTemplateView, self).get_context_data(**kwargs)
        context = {
            'news_items': News.objects.all(),
            'user': self.request.user,
        }
        return context


class NewsDetailView(DetailView):
    # TODO: rewrite this
    # https://docs.djangoproject.com/en/1.10/topics/class-based-views/mixins/#using-formmixin-with-detailview
    # http://stackoverflow.com/questions/16931901/django-combine-detailview-and-formview
    model = News
    template_name = u'news_item.html'
    form_class = CommentModelForm

    def get_context_data(self, **kwargs):
        context = super(NewsDetailView, self).get_context_data(**kwargs)
        form = self.form_class()
        self.object = self.get_object()
        comments = Comment.objects.filter(news=self.object)
        context.update({
            'form': form,
            'news': self.object,
            'comments': comments,
            'post_msg': False,
            'user': self.request.user,
        })
        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        News.objects.filter(slug=self.object.slug).update(viewed=F('viewed')+1)
        self.object.refresh_from_db()
        context = self.get_context_data(**kwargs)
        return render(self.request, self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        form_class = self.form_class
        self.object = self.get_object()
        form = form_class(self.object, self.request.user, self.request.POST)
        if form.is_valid():
            return self.form_valid(form, **kwargs)
        else:
            return self.form_invalid(form, **kwargs)

    def form_valid(self, form, **kwargs):
        form.save()
        form = self.form_class()
        context = self.get_context_data(**kwargs)
        context.update({
            'post_msg': True,
        })
        return render(self.request, self.template_name, context=context)

    def form_invalid(self, form, **kwargs):
        context = self.get_context_data(**kwargs)
        context.update({
            'post_msg': False,
        })
        return render(self.request, self.template_name, context=context)


class NewsByListView(ListView):
    template_name = u'news.html'
    context_object_name = 'news_items'


class NewsByViewedListView(NewsByListView):
    queryset = News.objects.all().order_by('-viewed')


class NewsByCommentsListView(NewsByListView):
    queryset = sorted(
        News.objects.all(),
        key=lambda n: n.comment_set.count(),
        reverse=True
    )


class NewsByLatestListView(NewsByListView):
    queryset = News.objects.all().order_by('-created')


class RegistrationFormView(FormView):
    template_name = u'registration.html'
    success_url = '/'
    form_class = RegistrationForm

    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context = self.get_context_data(**kwargs)
        context['form'] = form
        return render(self.request, self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form, **kwargs)
        else:
            return self.form_invalid(form, **kwargs)

    def form_valid(self, form, **kwargs):
        form.save()
        return super(RegistrationFormView, self).form_valid(form)


class LoginFormView(FormView):
    template_name = u'login.html'
    form_class = LoginForm

    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context = self.get_context_data(**kwargs)
        context['form'] = form
        return render(self.request, self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form, **kwargs)
        else:
            return self.form_invalid(form, **kwargs)

    def form_valid(self, form, **kwargs):
        user = authenticate(
            username=form.cleaned_data.get('name'),
            password=form.cleaned_data.get('password'),
        )
        if user is not None:
            if user.is_active:
                login(self.request, user)
                return HttpResponseRedirect(self.request.GET.get('next'))
        else:
            state = 'Пользователь и/или пароль неверные!'
        context = {
            'form': form,
            'state': state,
        }
        return render(self.request, self.template_name, context=context)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(request.GET.get('next'))


class PersonalListView(LoginRequiredMixin, ListView):
    login_url = '/login'
    template_name = u'personal.html'
    context_object_name = 'news_list'

    def get_queryset(self):
        return News.objects.filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(PersonalListView, self).get_context_data(**kwargs)
        td = timezone.now() - self.request.user.date_joined
        time_from_registration = '%s дн., %s час., %s секунд.' % (
            td.days, td.seconds // 3600, (td.seconds // 60) % 60)
        news_count = News.objects.filter(user=self.request.user).\
            aggregate(count=Count('user'))['count']
        comment_count = Comment.objects.filter(user=self.request.user).\
            aggregate(count=Count('user'))['count']
        days = td.days if td.days != 0 else 1
        comment_count_day = float(comment_count) / days
        context.update({
            'time_from_registration': time_from_registration,
            'comment_count': comment_count,
            'news_count': news_count,
            'comment_count_day': comment_count_day,
            'next_for_logout': True,
        })
        return context


class SinglePageListView(ListView):
    template_name = u'single_page.html'
    context_object_name = 'news_list'

    def get_queryset(self):
        return News.objects.all()

    def get_context_data(self, **kwargs):
        context = super(SinglePageListView, self).get_context_data(**kwargs)
        context.update({
            'next_for_logout': True,
        })
        return context


class JSONResponseMixin(object):
    def render_to_json_response(self, context, **response_kwargs):
        return JsonResponse(
            self.get_data(context), safe=False,
            **response_kwargs
        )

    def get_data(self, context):
        return context


class SinglePageJSONNewsListView(JSONResponseMixin, BaseListView):

    def get_queryset(self):
        return News.objects.values('id', 'name')

    def render_to_response(self, context, **response_kwargs):
        context = dict(news=list(self.object_list))
        return self.render_to_json_response(context, **response_kwargs)


class SinglePageJSONNewsDetailView(JSONResponseMixin, BaseDetailView):
    model = News

    def get_queryset(self):
        qs = super(SinglePageJSONNewsDetailView, self).get_queryset()
        return qs.values('id', 'text')

    def render_to_response(self, context, **response_kwargs):
        context = self.object
        return self.render_to_json_response(context, **response_kwargs)


class AjaxableResponseMixin(JSONResponseMixin):
    # TODO: get rid of this class or make 'result' data customizable
    def form_invalid(self, form):
        data = {'result': False, 'errors': form.errors}
        return self.render_to_response(data, status=400)

    def form_valid(self, form):
        if self.request.is_ajax():
            form.save()
            result = True
        else:
            result = False
        return self.render_to_response({'result': result})


class SinglePageJSONBookmarkCreateView(AjaxableResponseMixin, BaseCreateView):
    model = Bookmark
    form_class = BookmarkModelForm
    fields = ['user', 'news']

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.form_class
        form = form_class(self.request.POST)
        if form.is_valid():
            return self.form_valid(form, **kwargs)
        else:
            return self.form_invalid(form, **kwargs)

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)


class SinglePageJSONBookmarksListView(JSONResponseMixin, BaseListView):

    def get_queryset(self):
        return Bookmark.objects.values('news__id', 'news__name')

    def render_to_response(self, context, **response_kwargs):
        context = dict(news=list(self.object_list))
        return self.render_to_json_response(context, **response_kwargs)


class SinglePageJSONBookmarkExistsDetailView(JSONResponseMixin, View):

    def get_queryset(self):
        return self.model.objects.filter(
            news=self.request.GET.get('news'), user=self.request.user)

    def get(self, request, *args, **kwargs):
        is_exist = True
        if self.request.user.is_authenticated:
            try:
                Bookmark.objects.get(
                    news=self.request.GET.get('news'), user=self.request.user)
            except Bookmark.DoesNotExist:
                is_exist = False
        else:
            is_exist = False
        return self.render_to_json_response({'is_exist': is_exist})


class SinglePageJSONBookmarkDeleteView(JSONResponseMixin, BaseDeleteView):
    model = Bookmark

    def get_object(self):
        try:
            obj = self.model.objects.get(
                news=self.kwargs['news'], user=self.request.user)
            if not obj.user == self.request.user:
                obj = None
        except self.model.DoesNotExist:
            obj = None
        return obj

    def delete(self, request, *args, **kwargs):
        result = True
        self.object = self.get_object()
        if self.object:
            self.object.delete()
        else:
            result = False
        return self.render_to_response({'result': result})

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)
