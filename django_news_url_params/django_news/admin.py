from django.contrib import admin
from django_news.models import News, Comment


admin.site.register(News)
admin.site.register(Comment)
