from setuptools import setup

setup(name='day5',
      version='0.1',
      description='Tasks from day5',
      author='Artur Tukaev',
      license='MIT',
      packages=['day5', ],
      zip_safe=False)
