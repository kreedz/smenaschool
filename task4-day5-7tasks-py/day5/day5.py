# -*- coding: utf-8 -*-
from datetime import date


def get_sum_mult(lst):
    if not len(lst):
        return 0

    sum_ = 0
    for el, i in enumerate(lst):
        if i % 2 == 0:
            sum_ += el
    return sum_ * lst[-1]


def find_msg(s):
    ss = ''
    for c in s:
        if c.istitle():
            ss += c
    return ss


def is_3_words(s):
    count = 0
    for s in s.split():
        if not s.isalpha():
            break
        count += 1
    if count > 2:
        return True
    return False


def sub(*a):
    if len(a) == 0:
        return 0
    max_ = min_ = a[0]
    for i in a:
        if i > max_:
            max_ = i
        if i < min_:
            min_ = i
    return max_ - min_


def get_count_of_simple_numbers():
    c = 0
    for num in range(301):
        if num > 1:
            for i in range(2, num):
                if (num % i) == 0:
                    break
            else:
                c += 1
    return c


def get_sub_days(day1, day2):
    return abs((date(*day1) - date(*day2)).days)


def comments():
    comments = [
        {
            'id': 1,
            'parent_id': 0,
            'autor': 'Гарри',
            'time': '15:00',
            'text': 'Как перевернуть человека вверх ногами?',
        },
        {
            'id': 2,
            'parent_id': 1,
            'autor': 'Гермиона',
            'time': '15:05',
            'text': 'Левикорпус',
        },
        {
            'id': 3,
            'parent_id': 2,
            'autor': 'Гарри',
            'time': '15:15',
            'text': 'Спасибо сработало',
        },
        {
            'id': 4,
            'parent_id': 0,
            'autor': 'Рон',
            'time': '15:16',
            'text': 'Как спастись от левикорпуса?',
        },
        {
            'id': 5,
            'parent_id': 4,
            'autor': 'Полумна',
            'time': '15:17',
            'text': 'Либеракорпус',
        },
        {
            'id': 6,
            'parent_id': 3,
            'autor': 'Гермиона',
            'time': '15:22',
            'text': 'На ком пробуешь?',
        },
        {
            'id': 7,
            'parent_id': 4,
            'autor': 'Джинни',
            'time': '15:25',
            'text': 'Кто тебя обижает?',
        },
        {
            'id': 8,
            'parent_id': 7,
            'autor': 'Рон',
            'time': '15:26',
            'text': 'Гарри, кто же ещё?',
        },
        {
            'id': 9,
            'parent_id': 6,
            'autor': 'Гермиона',
            'time': '15:28',
            'text': 'А все поняла',
        },
    ]
    pr(comments)


def pr(comments, p=0, sep=2):
    for comment in comments:
        if comment['parent_id'] == p:
            print '-' * sep + comment['time'] + ' ' + comment['autor']
            print '-' * sep + comment['text']
            sep += 2
            pr(comments, p=comment['id'], sep=sep)


def set_task_to_run():
    while True:
        print '1. Сумма элементов массива с чётными элементами'
        print '2. Собрать заглавные элементы из строки'
        print '3. Три слова подряд в строке'
        print '4. Разница между макс и мин'
        print '5. Простые числа'
        print '6. Даты'
        print '7. Комментарии'
        task = int(raw_input('Введите номер задачи для выполнения: '))
        if task == 1:
            a = raw_input('Введите массив целых чисел через пробел: ').split()
            a = map(int, a)
            print get_sum_mult(a)
        elif task == 2:
            a = raw_input('Введите строку: ')
            print find_msg(a.decode('utf-8'))
        elif task == 3:
            a = raw_input('Введите строку: ')
            print is_3_words(a.decode('utf-8'))
        elif task == 4:
            a = raw_input('Введите массив чисел через пробел: ').split()
            a = map(float, a)
            result = sub(*a)
            if int(result) == result and type(result) == float:
                print int(result)
            else:
                print result
        elif task == 5:
            print get_count_of_simple_numbers()
        elif task == 6:
            dmsg = 'Введите %s дату в формате: ГГГГ ММ ДД: '
            d1 = raw_input(dmsg % 'первую').split()
            d1 = map(int, d1)
            d2 = raw_input(dmsg % 'вторую').split()
            d2 = map(int, d2)
            print get_sub_days(d1, d2)
        elif task == 7:
            comments()
        repeat = raw_input('Выбрать другую задачу? (д/н) ')
        if repeat != 'д':
            break


def run():
    s = '\n%s:\n'
    print s % 1, get_sum_mult([0, 1, 2, 3, 4, 5])
    print s % 2, find_msg(u'Asd Фыв Йцу')
    print s % 3, is_3_words(u'Hello World hello')
    print s % 3, is_3_words(u'Hello 123 World hello')
    print s % 4, sub(1, 2, 3)
    print sub(5, -5)
    print sub(10.2, -2.2, 0, 1.1, 0.5)
    print sub()
    print s % 5, get_count_of_simple_numbers()
    print s % 6, get_sub_days((1982, 4, 19), (1982, 4, 22))
    print s % 7
    comments()

if __name__ == '__main__':
    set_task_to_run()
